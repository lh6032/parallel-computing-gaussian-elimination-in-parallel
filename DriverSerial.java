/*
This program is driver class for GESerial.java
@author: SophieHou(lh6032)
 */

public class DriverSerial {
    public static void main(String[] args) throws Exception{
        new GESerial().main(args);
    }
}
