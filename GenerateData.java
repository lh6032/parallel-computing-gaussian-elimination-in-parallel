/*
This program generates matrix A,B
@author: SophieHou(lh6032)
 */

import java.util.Random;

public class GenerateData {
    static double [][] A;
    static double [] B;
    static double [] X;
    static int N = 40;
    long seed = 5;
    Random random = new Random(seed);

    public GenerateData(int N){
        this.N = N;
        A = new double [N*2][N*2];
        B = new double [N*2];
        X = new double [N*2];
    }

    public void generateInputData(){
        for (int col = 0; col < N; col++){
            for (int row = 0; row < N; row++){
                A[row][col] = random.nextDouble() * 100000;
            }
            B[col] = random.nextDouble()* 100000;
            X[col] = 0.0;
        }
    }

    public void printData(){
        int row, col;
        if (N <= 20){
            System.out.printf("\nA = \n\t");
            for (row=0; row<N; row++){
                for (col=0; col<N; col++){
                    System.out.printf("%5.2f%s", A[row][col], (col<N-1)? ",": ";\n\t");
                }
            }
            System.out.printf("\nB = [");
            for(col=0; col<N; col++){
                System.out.printf("%5.2f%s", B[col], (col<N-1)? ";": "]\n");
            }
        }
    }


}
