/*
This program runs Gaussian Elimination in parallel. It uses Parallel Java 2 library and
and it can detect the cores' number automatically.

@author: SophieHou(lh6032)
 */

import edu.rit.pj2.Loop;
import edu.rit.pj2.Task;

public class GEParallel extends Task{
    double maxVal = 0.0;
    static int N = 0, T = 1;
    static double[] X;

    // instance main program
    public void main(final String[] args) throws Exception{
        if (args.length < 1){
            new IllegalArgumentException("No argument error! Input 2 args: dim  thread");
        }
        N = Integer.parseInt(args[0]);  // dimension of matrix

        GenerateData gd = new GenerateData(N);
        gd.generateInputData();  // generate data

        System.out.println("\n\nBefore GE (Original Data):");
        gd.printData(); // print original data: A,B
        N = gd.N;
        X = new double[N];
        T = Integer.parseInt(args[1]);  // read threads number

        long start = System.currentTimeMillis();
        runGE(gd.A, gd.B, N);  // perform GE
        long end = System.currentTimeMillis();

        System.out.println("\n\nAfter GE:");
        printX();  // print X
        System.out.println("Runntime(ms): " + (end-start));   // check runGE() runtime


    }

    /*
    Runs Gaussian Elimination in parallel by call parallelFor(lb,ub).exec(new Loop()).
    */
    public void runGE(double[][] A, double[] B, int N){
        int maxRow = 0;
        for(int i = 0; i < N; i++){
            maxVal = A[i][i];
            maxRow=findMaxInCol(i, maxVal, A, B);
            swapRow(A[i],A[maxRow]);
            swapItem(B[i],B[maxRow]);
            final int rowI = i+1;
            final int m = i;

            // run in parallel, eliminate to 0.
            parallelFor(1, T).exec(new Loop(){
                public void run(int h) {
                    for (int colI = m; colI < N; colI++) {
                        A[rowI][colI] -= A[rowI][m] / A[m][m] * A[m][colI];
                    }
                    B[rowI] -= A[rowI][m] / A[m][m] * B[m];
                }
            });

            backSubstitute(A,B);  // calculate X variables
        }
    }

    /*
    Find the max element among multiple rows in each column, except the rows that already
    checked the max element. Return the row index which contains the max element.
     */
    public int findMaxInCol(int i, double maxVal, double[][] A, double[] B) {
        int j, maxRow = 0;
        for (j = i + 1; j < N; j++) {
            if (A[j][i] > maxVal) {
                maxRow = j;
            }
        }
        return maxRow;
    }

    /*
    Swap the maxRow in A with the top row which doean't contain the max element.
    */
    public void swapRow(double[] row1, double[] row2) {
        double[] temp = new double[N];
        temp = row1;
        row1 = row2;
        row2 = temp;
    }

    /*
    Swap B to match A.
     */
    public void swapItem(double item1, double item2) {
        double temp = 0.0;
        temp = item1;
        item1 = item2;
        item2 = temp;
    }

    /*
    Calculate unkown variables.
     */
    public void backSubstitute(double[][] A, double[] B) {
        double res = 0.0;
        for(int rowI = N-1; rowI >=0; rowI--){
            for(int colI = N-1; colI>rowI; colI--){
                res += A[rowI][colI]*X[colI];
            }
            X[rowI] = (B[rowI]-res)/A[rowI][rowI];  // calculate varibale
        }
    }

    /*
    Print X varibales.
     */
    public void printX(){
        System.out.print("\nSolution  X = [");
        for(int i = 0; i < N; i++){
            System.out.printf("%5.2f%s", X[i], (i<N-1)? ",": "");
        }
        System.out.println("]");
    }
}



