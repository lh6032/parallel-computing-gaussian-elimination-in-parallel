/*
This program is driver class for GEParallel.java
@author: SophieHou(lh6032)
 */
public class DriverParallel {
    public static void main(String[] args) throws Exception{
        new GEParallel().main(args);
    }
}
